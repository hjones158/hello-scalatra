FROM tomcat:7.0
MAINTAINER hjones
COPY target/*.war /usr/local/tomcat/webapps/hello-scalatra.war
EXPOSE 8080
CMD ["catalina.sh","run"]

